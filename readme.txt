MVControl project v0.1
======================

This project, still in construction, focus on the PureData-QT communication threw the network.
This tool is inspired by the work of errordeveloper (https://github.com/errordeveloper/qpd).
He works on the way to control interface for Pure Data from a QT application. I added the Pure Data to QT way.

With that tool I am trying to make a VJing tool processed with Pure Data and controlled with a QT interface.
All features can be learn with MIDI.

I use Qt Creator 2.4.1 Based on Qt 4.8.0 (32 bit) and PD 0.43.1-extended on a Linux Ubuntu 12.04LTS plateform
