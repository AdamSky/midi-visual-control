#ifndef QTMINI_H
#define QTMINI_H


#include <QtGui>
#include <QtNetwork>

class QtMini : public QMainWindow
{
// La macro Q_OBJECT est indispensable a toute classe heritant de QWidget
    Q_OBJECT

    QTabWidget *m_onglets;
    QWidget *m_page1, *m_page2;
    QWidget *g;
    QSlider *CrossFaderSlider;
    QPushButton  *createButton, *connectButton2, *connectButton;
    QSpinBox *pd_port_SpinBox2, *pd_port_SpinBox;

public :
    QtMini() ;
    QTcpServer *qpd_Serv2;
    QTcpSocket *socket2;
    QTcpSocket *qpd_Sock;

public slots:
    void connection2();
    void read2();
    void qpd_tcp_sock();
    void qpd_tcp_sock(int value);
    void connectToHost();

signals :
    void _qpd_new(QString _M);
    void _qpd_new8(QString);
    void _qpd_new2(QString);

};

#endif // QTMINI_H
