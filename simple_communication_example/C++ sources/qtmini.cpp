/**********************************************************************************************************/
/*                                       PD to QT example                                                 */
/*--------------------------------------------------------------------------------------------------------*/
/*                                      made by DUWYN Adam                                                */
/**********************************************************************************************************/

#include "qtmini.h"
#include <iostream>
using namespace std ;

QtMini::QtMini() : QMainWindow(0)
{
    g = new QWidget(this);              //create a widget
    QGridLayout *gl = new QGridLayout ; //create a grid
    g->setLayout(gl);
    resize(800,600);                    //size of the window
    m_onglets = new QTabWidget;         // creation of tabs

    m_page1 = new QWidget;              // tab1
    m_page2 = new QWidget;              // tab2

    m_onglets->addTab(m_page1, "Connections");  // tab1
    m_onglets->addTab(m_page2, "Mixer");        // tab2

    QGridLayout *m_page1gl = new QGridLayout;   // grid on each tab
    QGridLayout *m_page2gl = new QGridLayout;

    m_page1->setLayout(m_page1gl);
    m_page2->setLayout(m_page2gl);

    CrossFaderSlider = new QSlider(Qt::Horizontal);             //CrossFader
    m_page2gl           ->  addWidget(CrossFaderSlider,0,0);    //Position of the CrossFader
    CrossFaderSlider    ->  setObjectName("Crossfdr");
    CrossFaderSlider    ->  setRange(0,127);
    CrossFaderSlider    ->  setValue(0);
    connect(CrossFaderSlider, SIGNAL(valueChanged(int)), this, SLOT(qpd_tcp_sock(int)));

    createButton    =   new QPushButton("Create / Destroy");    //PushButton
    m_page2gl       ->  addWidget(createButton,0,1);            //Position of the Button
    createButton->setObjectName("create");
    connect(createButton, SIGNAL(clicked()), this, SLOT(qpd_tcp_sock()));


    //************************* CONNECTIONS TAB *********************************

    QLineEdit *pd_host_LineEdit2 = new QLineEdit("localhost");
    m_page1gl->addWidget(pd_host_LineEdit2, 1, 1);

    pd_port_SpinBox = new QSpinBox();
    pd_port_SpinBox->setRange(1, 65535);
    pd_port_SpinBox->setValue(2030);
    m_page1gl->addWidget(pd_port_SpinBox, 0, 2);

    pd_port_SpinBox2 = new QSpinBox();
    pd_port_SpinBox2->setRange(1, 65535);
    pd_port_SpinBox2->setValue(2021);
    m_page1gl->addWidget(pd_port_SpinBox2, 1, 2);

    connectButton = new QPushButton("ConnectToHost");
    m_page1gl->addWidget(connectButton, 0, 3);
    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectToHost())); //connection QT to PD

    QLabel *outStream = new QLabel("OUT STREAM");
    m_page1gl->addWidget(outStream, 1, 0);

    QLabel *inStream = new QLabel("IN STREAM");
    m_page1gl->addWidget(inStream, 0, 0);

    QLineEdit *pd_host_LineEdit = new QLineEdit("localhost");
    m_page1gl->addWidget(pd_host_LineEdit, 0, 1);

    connectButton2 = new QPushButton("ConnectToHost");
    connectButton2->setObjectName("connect");
    m_page1gl->addWidget(connectButton2, 1, 3);
    connect(connectButton2, SIGNAL(clicked()), this, SLOT(qpd_tcp_sock())); //connection PD to QT

    //****************************Partie serveur**************************************

    qpd_Sock = new QTcpSocket();
    qpd_Serv2 = new QTcpServer(this);

       if (!qpd_Serv2->listen(QHostAddress::Any, pd_port_SpinBox2->value()))
           {
           qDebug()<< "is not open";
           }
           else
           {
           qDebug()<< "is open";
           connect(qpd_Serv2, SIGNAL(newConnection()), this, SLOT(connection2()));
           }
       setCentralWidget(g); //widget central de la fenetre
       gl->addWidget(m_onglets, 0, 0 );
     }

void QtMini::connection2()
{
    socket2 = qpd_Serv2->nextPendingConnection();
    connect(socket2, SIGNAL(readyRead()), this, SLOT(read2()));
}

void QtMini::read2() //read and interpret values comming from PD
{
   // QString ligne = socket2->readLine();
        disconnect(CrossFaderSlider, 0, 0, 0);
    QByteArray array = socket2->readAll();
    QString ligne = array.data();
    qDebug()<<"ligne recue : "<<ligne;

    int number=ligne.count(';',(Qt::CaseSensitivity)0); //number of lines of the instruction (severals lines sometimes)
    qDebug()<<"number of lignes : "<<number;

    QString ligneInstruction = ligne.section("",1,10);  //keep instruction
    qDebug()<<"instruction : " << ligneInstruction;

   for(int k=0;k<number;k++) //Because values comes sometimes several by several
    {
    QString first = ligne.section(';',0,0);
    ligne = ligne.section(';',1,number-1);
    qDebug()<<"first : " << first;

    QString ligneTronquee = first.section('/', 1, 1); //keep the number
     qDebug()<<"value : "<<ligneTronquee;

    QString ligneTronquee2;

     if (ligneInstruction == "Slidervalu")
        {
         if (ligneTronquee.size() == 2)
            {
             ligneTronquee2 = ligneTronquee.section("", 1,2);
             qDebug()<<"value between 0 and 9 : " << ligneTronquee2;
            }
         else if (ligneTronquee.size() == 3)
            {
             ligneTronquee2 = ligneTronquee.section("", 1,3);
             qDebug()<<"value between 10 and 99 : "<< ligneTronquee2;
            }
         else
            {
             ligneTronquee2 = ligneTronquee.section("", 1,4);
            qDebug()<<"value between 100 and 127 : "<<ligneTronquee2;
            }
         int a = ligneTronquee2.toInt();
         //qDebug()<<a;
        CrossFaderSlider->setValue(a);
        }
     }
     //qDebug()<< socket2->bytesAvailable();*/

       connect(CrossFaderSlider, SIGNAL(valueChanged(int)), this, SLOT(qpd_tcp_sock(int)));

}

void QtMini::qpd_tcp_sock(void)
{
    QObject *A = sender();
    QString n;
    if (A)
        n = A->objectName();
    if (n.isEmpty())
        n = QString("unknown");
    qDebug()<<n;
    n.append(";\n");
    qpd_Sock->write(n.toAscii());
    return;
}

void QtMini::qpd_tcp_sock(int value)
{
    QObject *B = sender();
    QString n;
    if (B)
        n = B->objectName();
    if (n.isEmpty())
        n = QString("unknown");
    n = n + QString(" ") + QString::number(value);
    qDebug()<<n;
    n.append(";\n");
    qpd_Sock->write(n.toAscii());
    return;
}

void QtMini::connectToHost()
{
    qpd_Sock->connectToHost("Localhost", pd_port_SpinBox->value());
    if (!qpd_Sock->isOpen())
        {
        qDebug()<< "is not open";
        }
        else
        {
        qDebug()<< "is open";
        }
}



