#include <QApplication>

#include "qtmini.h"
int main(int argc, char **argv)
{
    QApplication app(argc, argv, "application Qt minimale");
    QtMini *wid = new QtMini;
    wid->setWindowTitle("MIDI Visual Control");
    wid->show();

    return app.exec();
}
