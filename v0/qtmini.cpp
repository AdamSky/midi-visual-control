#include "qtmini.h"
#include <iostream>
using namespace std ;

QtMini::QtMini() : QMainWindow(0)
{
    g = new QWidget(this);              //on cree un QWidget
    QGridLayout *gl = new QGridLayout ; //on cree une grille
    g->setLayout(gl);
    resize(800,600);
    m_onglets = new QTabWidget;        // creation du QTabWidget pour afficher les onglets

    m_page1 = new QWidget;             // creations des QWidgets qui vont accueillir les layouts
    m_page2 = new QWidget;
    m_page3 = new QWidget;
    m_page4 = new QWidget;

    m_onglets->addTab(m_page1, "Connections");  //onglet1
    m_onglets->addTab(m_page2, "Mixer");        //onglet2
    m_onglets->addTab(m_page3, "Console");      //onglet3
    m_onglets->addTab(m_page4, "Video assignation"); //onglet4

    QGridLayout *m_page1gl = new QGridLayout;
    QGridLayout *m_page2gl = new QGridLayout;
    QGridLayout *m_page3gl = new QGridLayout;
    QGridLayout *m_page4gl = new QGridLayout;

    m_page1->setLayout(m_page1gl);
    m_page2->setLayout(m_page2gl);
    m_page3->setLayout(m_page3gl);
    m_page4->setLayout(m_page4gl);

    connect(this, SIGNAL(_qpd_new(QString)), this, SLOT(qpd_tcp_w(QString)));
    connect(this, SIGNAL(_qpd_new8(QString)), this, SLOT(qpd_tcp_w(QString)));
    //**************************ONGLET CROSSFADER******************************************

            //-----------------------QSliders-----------------------

    CrossFaderSlider = new QSlider(Qt::Horizontal);
    R_left  =   new QSlider(Qt::Horizontal);            V_left  =   new QSlider(Qt::Horizontal);
    B_left  =   new QSlider(Qt::Horizontal);            A_left  =   new QSlider(Qt::Horizontal);
    R_right =   new QSlider(Qt::Horizontal);            V_right =   new QSlider(Qt::Horizontal);
    B_right =   new QSlider(Qt::Horizontal);            A_right =   new QSlider(Qt::Horizontal);

    Sliders <<  CrossFaderSlider <<  R_left << V_left <<  B_left <<  A_left <<  R_right << V_right <<  B_right <<  A_right;

    int const tailleTableau2(9);
    QString sliderNames[tailleTableau2] = {"Crossfdr" , "Rvalueleft" , "Vvalueleft" , "Bvalueleft" , "Avalueleft" ,
                                           "Rvalueright" , "Vvalueright" , "Bvalueright" , "Avalueright" };
    for(int i=0; i < Sliders.size(); i++)
    {
        Sliders[i]->setRange(0,127);
        Sliders[i]->setValue(127);
        CrossFaderSlider->setValue(0);
        Sliders[i]->setObjectName(sliderNames[i]);
        connect(Sliders[i], SIGNAL(valueChanged(int)), this, SLOT(qpd_tcp_sock(int)));
    }

        //-----------------------QPushButton-----------------------

    createButton    =   new QPushButton("Create / Destroy");
    VideoLeft       =   new QPushButton("Video 1");             VideoRight      =   new QPushButton("Video 2");
    play1           =   new QPushButton("play / stop");         play2           =   new QPushButton("play / stop");
    go              =   new QPushButton("GO ! / STOP Render");
    connectButton   =   new QPushButton("ConnectToHost");

    kaleidoscope1   =   new QPushButton("K effetcs");           kaleidoscope2   =   new QPushButton("K effetcs");
    backlight1      =   new QPushButton("Backlight");           backlight2      =   new QPushButton("Backlight");

    QList<QPushButton*> pushButton;
    pushButton << createButton << VideoLeft << VideoRight << play1 << play2 << go << connectButton;

    for(int i=0; i < pushButton.size(); i++)
        connect(pushButton[i], SIGNAL(clicked()), this, SLOT(qpd_tcp_sock()));

    QList<QPushButton *> effectsButton;
    effectsButton   <<  kaleidoscope1 <<  kaleidoscope2 <<  backlight1 <<  backlight2;

    for(int i=0; i < effectsButton.size(); i++)
        connect(effectsButton[i], SIGNAL(clicked()), this, SLOT(openWindow()));

    int const tailleTableau3(39);
    QString ButtonNames[tailleTableau3] = {"create" ,"videoleft" , "videoright" ,"play1" , "play2" , "go" , "connect" ,
                                           "lrnopenl" , "lrnrleft" , "lrnvleft" , "lrnbleft" , "lrnaleft" ,"lrnopenr" , "lrnrright" , "lrnvright" ,"lrnbright" , "lrnaright" ,
                                           "lrnplayr" , "lrnplayl" , "lrncreate", "lrncrossfdr", "lrnkeffect1" , "lrnkeffect2" , "lrnbacklight1" , "lrnbacklight2" ,
                                           "lrndol" , "lrnrel" , "lrnmil" , "lrnfal" , "lrnsoll" , "lrnlal" , "lrnsil" , "lrndor" , "lrnrer" , "lrnmir" , "lrnfar" , "lrnsolr" , "lrnlar" , "lrnsir"};
    for (int i = 0; i<7; i++)
        pushButton[i]->setObjectName(ButtonNames[i]);

        //-----------------------QCheckBox-----------------------

    checkbox_openVL     = new QCheckBox;    checkbox_R_left     = new QCheckBox;    checkbox_V_left     = new QCheckBox;
    checkbox_B_left     = new QCheckBox;    checkbox_A_left     = new QCheckBox;    checkbox_openVR     = new QCheckBox;
    checkbox_R_right    = new QCheckBox;    checkbox_V_right    = new QCheckBox;    checkbox_B_right    = new QCheckBox;
    checkbox_A_right    = new QCheckBox;    checkbox_playR      = new QCheckBox;    checkbox_playL      = new QCheckBox;
    checkbox_create     = new QCheckBox;    CrossFader_checkBox = new QCheckBox;
    checkbox_Keffect1   = new QCheckBox;    checkbox_Keffect2   = new QCheckBox;    checkbox_backlight1 = new QCheckBox;
    checkbox_backlight2 = new QCheckBox;

    //-----------------------QCheckBox de l'onglet "video choice"-----------------------

   checkbox_Dol = new QCheckBox;       checkbox_Dor = new QCheckBox;
   checkbox_Rel = new QCheckBox;       checkbox_Rer = new QCheckBox;
   checkbox_Mil = new QCheckBox;       checkbox_Mir = new QCheckBox;
   checkbox_Fal = new QCheckBox;       checkbox_Far = new QCheckBox;
   checkbox_Soll = new QCheckBox;      checkbox_Solr = new QCheckBox;
   checkbox_Lal = new QCheckBox;       checkbox_Lar = new QCheckBox;
   checkbox_Sil = new QCheckBox;       checkbox_Sir = new QCheckBox;

    QList<QCheckBox*> checkBox;
    checkBox << checkbox_openVL  << checkbox_R_left  << checkbox_V_left  << checkbox_B_left  << checkbox_A_left << checkbox_openVR
             << checkbox_R_right << checkbox_V_right << checkbox_B_right << checkbox_A_right << checkbox_playR  << checkbox_playL
             << checkbox_create  << CrossFader_checkBox << checkbox_Keffect1 << checkbox_Keffect2
             << checkbox_backlight1 << checkbox_backlight2 << checkbox_Dol << checkbox_Rel << checkbox_Mil << checkbox_Fal
             << checkbox_Soll    << checkbox_Lal     << checkbox_Sil     << checkbox_Dor     << checkbox_Rer    << checkbox_Mir
             << checkbox_Far     << checkbox_Solr    << checkbox_Lar     << checkbox_Sir;

    for(int i = 0; i<checkBox.size(); i++)
        checkBox[i]->setObjectName(ButtonNames[i + 7]);

    for(int i=0; i < checkBox.size(); i++)
        connect(checkBox[i], SIGNAL(clicked()), this, SLOT(qpd_tcp_sock()));

    //-----------------------QLabels----------------------------

    QLabel *R_l = new QLabel(tr("R")); R_l->setAlignment(Qt::AlignCenter);
    QLabel *V_l = new QLabel(tr("V")); V_l->setAlignment(Qt::AlignCenter);
    QLabel *B_l = new QLabel(tr("B")); B_l->setAlignment(Qt::AlignCenter);
    QLabel *A_l = new QLabel(tr("A")); A_l->setAlignment(Qt::AlignCenter);

    //**************************ONGLET "VIDEO CHOICE"******************************

        //-----------------------QPushButton-----------------------

    chooseLeftVid = new QPushButton("choose left video");
    chooseRightVid = new QPushButton("choose right video");
    resetRightVid = new QPushButton("reset right vid");
    resetLeftVid = new QPushButton("reset left vid");

    QList<QPushButton*> chooseVid;
    chooseVid << chooseLeftVid << chooseRightVid << resetLeftVid << resetRightVid;

    for(int i=0; i < chooseVid.size(); i++)
        connect(chooseVid[i], SIGNAL(clicked()), this, SLOT(openInstructionWindow()));

    int const tailleTableau4(4);
    QString ButtonNames2[tailleTableau4] = {"chooseLeftVid","chooseRightVid", "resetLeftVid" , "resetRightVid"};

    for (int i = 0; i<tailleTableau4; i++)
        chooseVid[i]->setObjectName(ButtonNames2[i]);

    viewVideoNameLeft = new QTextEdit;
    viewVideoNameRight = new QTextEdit;

    //*******************************POSITIONS***************************************

    QGroupBox *groupRightParams = new QGroupBox("Right Params", m_page2);
    m_page2gl           ->  addWidget(groupRightParams,2,3);
    QVBoxLayout *VBoxRight = new QVBoxLayout;
    VBoxRight           ->  addWidget(R_right);
    VBoxRight           ->  addWidget(V_right);
    VBoxRight           ->  addWidget(B_right);
    VBoxRight           ->  addWidget(A_right);
    VBoxRight           ->  insertSpacing(4,30);
    VBoxRight           ->  addWidget(VideoRight);
    VBoxRight           ->  addWidget(play2);
    groupRightParams    ->  setLayout(VBoxRight);

    QGroupBox *groupLeftParams = new QGroupBox("Left Params", m_page2);
    m_page2gl           ->  addWidget(groupLeftParams,2,1);
    QVBoxLayout *VBoxLeft = new QVBoxLayout;
    VBoxLeft            ->  addWidget(R_left);
    VBoxLeft            ->  addWidget(V_left);
    VBoxLeft            ->  addWidget(B_left);
    VBoxLeft            ->  addWidget(A_left);
    VBoxLeft            ->  insertSpacing(4,30);
    VBoxLeft            ->  addWidget(VideoLeft);
    VBoxLeft            ->  addWidget(play1);
    groupLeftParams     ->  setLayout(VBoxLeft);

    QGroupBox *groupRightCheckBox = new QGroupBox("learn", m_page2);
    m_page2gl           ->  addWidget(groupRightCheckBox,2,4);
    QVBoxLayout *VBoxRightlearn = new QVBoxLayout;
    VBoxRightlearn      ->  addWidget(checkbox_R_right);
    VBoxRightlearn      ->  addWidget(checkbox_V_right);
    VBoxRightlearn      ->  addWidget(checkbox_B_right);
    VBoxRightlearn      ->  addWidget(checkbox_A_right);
    VBoxRightlearn      ->  insertSpacing(4,35);
    VBoxRightlearn      ->  addWidget(checkbox_openVR);
    VBoxRightlearn      ->  addWidget(checkbox_playR);
    groupRightCheckBox  ->  setLayout(VBoxRightlearn);

    QGroupBox *groupLeftCheckBox = new QGroupBox("learn", m_page2);
    m_page2gl           ->  addWidget(groupLeftCheckBox,2,0);
    QVBoxLayout *VBoxLeftlearn = new QVBoxLayout;
    VBoxLeftlearn       ->  addWidget(checkbox_R_left);
    VBoxLeftlearn       ->  addWidget(checkbox_V_left);
    VBoxLeftlearn       ->  addWidget(checkbox_B_left);
    VBoxLeftlearn       ->  addWidget(checkbox_A_left);
    VBoxLeftlearn       ->  insertSpacing(4,35);
    VBoxLeftlearn       ->  addWidget(checkbox_openVL);
    VBoxLeftlearn       ->  addWidget(checkbox_playL);
    groupLeftCheckBox   ->  setLayout(VBoxLeftlearn);

    QGroupBox *groupLeftLabels = new QGroupBox("", m_page2);
    m_page2gl           ->  addWidget(groupLeftLabels,2,2);
    QVBoxLayout *VBoxLabels = new QVBoxLayout;
    VBoxLabels          ->  addWidget(R_l);
    VBoxLabels          ->  addWidget(V_l);
    VBoxLabels          ->  addWidget(B_l);
    VBoxLabels          ->  addWidget(A_l);
    VBoxLabels          ->  insertSpacing(4,92);
    groupLeftLabels     ->  setLayout(VBoxLabels);

      QGroupBox *groupMixer = new QGroupBox("CrossFader", m_page2);
      m_page2gl         ->  addWidget(groupMixer,1,1,1,3);
      QHBoxLayout *HBox1 = new QHBoxLayout;
      HBox1             ->  addWidget(CrossFader_checkBox);
      HBox1             ->  addWidget(CrossFaderSlider);
      groupMixer        ->  setLayout(HBox1);

      QGroupBox *groupCreation = new QGroupBox("Create/Destroy window", m_page2);
      m_page2gl         ->  addWidget(groupCreation,0,2,1,1);
      QHBoxLayout *CreateBox = new QHBoxLayout;
      CreateBox         ->  addWidget(checkbox_create);
      CreateBox         ->  addWidget(createButton);
      groupCreation     ->  setLayout(CreateBox);

      QGroupBox *groupEffectsLeft = new QGroupBox("Left Effects", m_page2);
      m_page2gl         ->  addWidget(groupEffectsLeft,6,1);
      QVBoxLayout *VBoxLeftEffects = new QVBoxLayout;
      VBoxLeftEffects   ->  addWidget(kaleidoscope1);
      VBoxLeftEffects   ->  addWidget(backlight1);
      groupEffectsLeft  ->  setLayout(VBoxLeftEffects);

      QGroupBox *groupEffectsLearnLeft = new QGroupBox("", m_page2);
      m_page2gl         -> addWidget(groupEffectsLearnLeft,6,0);
      QVBoxLayout *VBoxLeftEffectsLearn = new QVBoxLayout;
      VBoxLeftEffectsLearn->addWidget(checkbox_Keffect1);
      VBoxLeftEffectsLearn->addWidget(checkbox_backlight1);
      groupEffectsLearnLeft->setLayout(VBoxLeftEffectsLearn);

      QGroupBox *groupEffectsRight = new QGroupBox("Right Effects", m_page2);
      m_page2gl         ->  addWidget(groupEffectsRight,6,3);
      QVBoxLayout *VBoxRightEffects = new QVBoxLayout;
      VBoxRightEffects  ->  addWidget(kaleidoscope2);
      VBoxRightEffects  ->  addWidget(backlight2);
      groupEffectsRight ->  setLayout(VBoxRightEffects);

      QGroupBox *groupEffectsLearnRight = new QGroupBox("", m_page2);
      m_page2gl         -> addWidget(groupEffectsLearnRight,6,4);
      QVBoxLayout *VBoxRightEffectsLearn = new QVBoxLayout;
      VBoxRightEffectsLearn->addWidget(checkbox_Keffect2);
      VBoxRightEffectsLearn->addWidget(checkbox_backlight2);
      groupEffectsLearnRight->setLayout(VBoxRightEffectsLearn);

          m_page4gl  ->  addWidget(chooseRightVid, 0, 1);
          m_page4gl  ->  addWidget(resetRightVid, 1, 1);
          m_page4gl  ->  addWidget(viewVideoNameRight, 2, 1);

      QGroupBox *checkBoxPlacementRight = new QGroupBox("Learn" , m_page4);
      m_page4gl         ->  addWidget(checkBoxPlacementRight, 3, 1);
      QHBoxLayout *checkBoxPlacementBoxRight = new QHBoxLayout;
      for(int i = 25; i<checkBox.size(); i++)
      checkBoxPlacementBoxRight->addWidget(checkBox[i]);
      checkBoxPlacementRight->setLayout(checkBoxPlacementBoxRight);

          m_page4gl  ->  addWidget(chooseLeftVid, 0, 0);
          m_page4gl  ->  addWidget(resetLeftVid, 1, 0);
          m_page4gl  ->  addWidget(viewVideoNameLeft, 2, 0);

      QGroupBox *checkBoxPlacementLeft = new QGroupBox("Learn" , m_page4);
      m_page4gl         ->  addWidget(checkBoxPlacementLeft, 3, 0);
      QHBoxLayout *checkBoxPlacementBoxLeft = new QHBoxLayout;
      for(int i = 18; i<25; i++)
      checkBoxPlacementBoxLeft->addWidget(checkBox[i]);
      checkBoxPlacementLeft->setLayout(checkBoxPlacementBoxLeft);

      m_page2gl->addWidget(go, 5,2);

    //*************************ONGLET CONNECTIONS*********************************

    QLineEdit *pd_host_LineEdit2 = new QLineEdit("localhost");
    m_page1gl->addWidget(pd_host_LineEdit2, 1, 1);

    pd_port_SpinBox = new QSpinBox();
    pd_port_SpinBox->setRange(1, 65535);
    pd_port_SpinBox->setValue(2030);
    m_page1gl->addWidget(pd_port_SpinBox, 0, 2);

    pd_port_SpinBox2 = new QSpinBox();
    pd_port_SpinBox2->setRange(1, 65535);
    pd_port_SpinBox2->setValue(2021);
    m_page1gl->addWidget(pd_port_SpinBox2, 1, 2);

    //connectButton = new QPushButton("ConnectToHost"); mis avec la liste de boutons
    m_page1gl->addWidget(connectButton, 0, 3);
    connect(connectButton, SIGNAL(clicked()), this, SLOT(connectToHost()));

    QLabel *outStream = new QLabel("OUT STREAM");
    m_page1gl->addWidget(outStream, 1, 0);

    QLabel *inStream = new QLabel("IN STREAM");
    m_page1gl->addWidget(inStream, 0, 0);

    QLineEdit *pd_host_LineEdit = new QLineEdit("localhost");
    m_page1gl->addWidget(pd_host_LineEdit, 0, 1);

    connectButton2 = new QPushButton("ConnectToHost");
    m_page1gl->addWidget(connectButton2, 1, 3);
    connect(connectButton2, SIGNAL(clicked()), this, SLOT(qpd_tcp_sock()));

    //**************************ONGLET CONSOLE*************************************

    Console = new QTextEdit();
    m_page3gl->addWidget(Console, 0, 0);

    //****************************Partie serveur**************************************

    qpd_Sock = new QTcpSocket();
    qpd_Serv2 = new QTcpServer(this);

       if (!qpd_Serv2->listen(QHostAddress::Any, pd_port_SpinBox2->value()))
           {
           QString a = QString::number(pd_port_SpinBox2->value());
           Console->insertPlainText("port" + a + " out \n");
           }
           else
           {
           QString a = QString::number(pd_port_SpinBox2->value());
           Console->insertPlainText("port " + a + " OK \n");
           connect(qpd_Serv2, SIGNAL(newConnection()), this, SLOT(connection2()));
           }
       setCentralWidget(g); //widget central de la fenetre
       gl->addWidget(m_onglets, 0, 0 );
     }

void QtMini::connection2()
{
    socket2 = qpd_Serv2->nextPendingConnection();
    connect(socket2, SIGNAL(readyRead()), this, SLOT(read2()));
}

void QtMini::read2()
{
   // QString ligne = socket2->readLine();
    for(int i=0; i < Sliders.size(); i++)
    {
        disconnect(Sliders[i], 0, 0, 0);
    }
    QByteArray array = socket2->readAll();
    QString ligne = array.data();
    qDebug()<<"ligne recue : "<<ligne;

    int number=ligne.count(';',(Qt::CaseSensitivity)0);
    qDebug()<<"number of lignes : "<<number;

    QString ligneInstruction = ligne.section("",1,10);
    qDebug()<<"instruction : " << ligneInstruction;

   for(int k=0;k<number;k++)
    {
    QString first = ligne.section(';',0,0);
    ligne = ligne.section(';',1,number-1);
    qDebug()<<"first : " << first;

    QString ligneTronquee = first.section('/', 1, 1);
     qDebug()<<"valeur numerique : "<<ligneTronquee;

    QString ligneTronquee2;

     int const tailleTableau(9);
     QString tableau[tailleTableau] = {"Slidervalu" , "Rvalueleft" , "Vvalueleft" , "Bvalueleft",
                                       "Avalueleft" , "Rvaluerigh" , "Vvaluerigh" , "Bvaluerigh" , "Avaluerigh" };
     QList<QSlider*> listLabel;
     listLabel << CrossFaderSlider;
     listLabel << R_left;   listLabel << V_left;    listLabel << B_left;    listLabel << A_left;
     listLabel << R_right;  listLabel << V_right;   listLabel << B_right;   listLabel << A_right;

     for(int i = 0; i < tailleTableau; i++)
     {
         if (ligneInstruction == tableau[i])
        {
         if (ligneTronquee.size() == 2)
            {
             ligneTronquee2 = ligneTronquee.section("", 1,2);
             qDebug()<<"valeur entre 0 et 9 : " << ligneTronquee2;
            }
         else if (ligneTronquee.size() == 3)
            {
             ligneTronquee2 = ligneTronquee.section("", 1,3);
             qDebug()<<"valeur entre 10 et 99 : "<< ligneTronquee2;
            }
         else
            {
             ligneTronquee2 = ligneTronquee.section("", 1,4);
            //qDebug()<<ligneTronquee2;
            }
         int a = ligneTronquee2.toInt();
         //qDebug()<<a;
         listLabel[i]->setValue(a);
        }
     }
    }
     //qDebug()<< socket2->bytesAvailable();*/
   for(int i=0; i < Sliders.size(); i++)
   {
       connect(Sliders[i], SIGNAL(valueChanged(int)), this, SLOT(qpd_tcp_sock(int)));
   }
}

void QtMini::qpd_tcp_sock(void)
{
    QObject *A = sender();
    QString n;
    if (A)
        n = A->objectName();
    if (n.isEmpty())
        n = QString("unknown");
    //qDebug()<<n;
            _qpd_new8(n);
}

void QtMini::qpd_tcp_w(QString _qpd_m)
{
   _qpd_m.append(";\n");
   qpd_Sock->write(_qpd_m.toAscii());
   return;
}

void QtMini::qpd_tcp_sock(int value)
{
    QObject *B = sender();
    QString n;
    if (B)
        n = B->objectName();
    if (n.isEmpty())
        n = QString("unknown");
    n = n + QString(" ") + QString::number(value);
    //qDebug()<<n;
            _qpd_new8(n);
}

void QtMini::connectToHost()
{
     qpd_Sock->connectToHost("Localhost", pd_port_SpinBox->value());
    if (!qpd_Sock->isOpen())
        {
        QString a = QString::number(pd_port_SpinBox->value());
        Console->insertPlainText("port" + a + " out \n onnection refusee \n");
        }
        else
        {
        QString a = QString::number(pd_port_SpinBox->value());
        Console->insertPlainText("port " + a + " OK \n");
        }
}

void QtMini::openWindow()
{
    QObject *A = sender();
    QPushButton *B = dynamic_cast<QPushButton *> (A);
    B->setEnabled(false);

    if (B == kaleidoscope1){
        k1 = new kaleidoscope("left kaleidoscope effect", "kparam1l", "kparam2l", "kparam3l", "kparam4l", "kparam5l", "kparam6l", "kparam7l",
                              "lrnk1l" , "lrnk2l" , "lrnk3l" , "lrnk4l" , "lrnk5l" , "lrnk6l" , "lrnk7l");
        connect(k1, SIGNAL(kParamChanged(QString)), this, SLOT(qpd_tcp_w(QString)));
        connect(k1, SIGNAL(close()), this, SLOT(activateButton()));}

    if (B == kaleidoscope2){
        k2 = new kaleidoscope("right kaleidoscope effect", "kparam1r", "kparam2r" , "kparam3r", "kparam4r", "kparam5r" , "kparam6r", "kparam7r",
                              "lrnk1r" , "lrnk2r" , "lrnk3r" , "lrnk4r" , "lrnk5r" , "lrnk6r" , "lrnk7r");
        connect(k2, SIGNAL(kParamChanged(QString)), this, SLOT(qpd_tcp_w(QString)));
        connect(k2, SIGNAL(close()), this, SLOT(activateButton()));}

    if (B == backlight2){
        b1 = new backlight("backlightR", "bparam1r" , "bparam2r" , "bparam3r" , "lrnb1r" , "lrnb2r" , "lrnb3r");
        connect(b1, SIGNAL(kParamChanged(QString)), this, SLOT(qpd_tcp_w(QString)));
        connect(b1, SIGNAL(close()), this, SLOT(activateButton()));}

    if (B ==backlight1){
        b2 = new backlight("backlightL", "bparam1l" , "bparam2l" , "bparam3l" , "lrnb1l" , "lrnb2l" , "lrnb3l");
        connect(b2, SIGNAL(kParamChanged(QString)), this, SLOT(qpd_tcp_w(QString)));
        connect(b2, SIGNAL(close()), this, SLOT(activateButton()));}
}

void QtMini::activateButton()
{
    QObject *A = sender();
    kaleidoscope *B = dynamic_cast<kaleidoscope *> (A);
    backlight *C = dynamic_cast<backlight *> (A);

    if (B == k1)    kaleidoscope1->setEnabled(true);
    if (B == k2)    kaleidoscope2->setEnabled(true);
    if (C == b1)    backlight2->setEnabled(true);
    if (C == b2)    backlight1->setEnabled(true);
}

void QtMini::openInstructionWindow()
{
    QObject *A = sender();
    QPushButton *B = dynamic_cast<QPushButton *> (A);
    QString fichier;
    QString n = B->objectName();
    if (A == chooseLeftVid)
    {
        fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Video (*)");
        viewVideoNameLeft->insertPlainText(fichier + "\n");
        n = n + " symbol " + fichier;
    }
    if (A == chooseRightVid)
    {
        fichier = QFileDialog::getOpenFileName(this, "Ouvrir un fichier", QString(), "Video (*)");
        viewVideoNameRight->insertPlainText(fichier + "\n");
         n = n + " symbol " + fichier;
    }
    if (A == resetLeftVid)          viewVideoNameLeft->clear();
    if (A == resetRightVid)         viewVideoNameRight->clear();
    if (n.isEmpty())
        n = QString("unknown");
    //qDebug()<<n;
            _qpd_new8(n);

}





