#ifndef KALEIDOSCOPE_H
#define KALEIDOSCOPE_H

#include <QtGui>

class kaleidoscope : public QMainWindow
{
    Q_OBJECT
    QWidget *h;
    QDial *Kparam1, *Kparam2, *Kparam3, *Kparam4, *Kparam5, *Kparam6, *Kparam7;

public:
    kaleidoscope(const QString &titre, const QString &prefixe1, const QString &prefixe2, const QString &prefixe3, const QString &prefixe4,
                 const QString &prefixe5, const QString &prefixe6, const QString &prefixe7, const QString &lrn1, const QString &lrn2,
                 const QString &lrn3, const QString &lrn4, const QString &lrn5, const QString &lrn6, const QString &lrn7);
    QSpinBox *getKparam1();
    void closeEvent(QCloseEvent *event);

private:
    QString prefixe1, prefixe2, prefixe3, prefixe4, prefixe5, prefixe6, prefixe7, lrn1 , lrn2, lrn3, lrn4, lrn5, lrn6, lrn7;

public slots:

private slots:
    void valueChanged(int);
    void valueChanged(void);

signals:
    void kParamChanged(const QString &);
    void close();


};

#endif // KALEIDOSCOPE_H
