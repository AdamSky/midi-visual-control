#include "kaleidoscope.h"
#include "qtmini.h"

kaleidoscope::kaleidoscope(const QString & titre, const QString & prefixe1, const QString & prefixe2, const QString & prefixe3, const QString &prefixe4,
                           const QString &prefixe5, const QString &prefixe6, const QString &prefixe7, const QString &lrn1, const QString &lrn2,
                           const QString &lrn3, const QString &lrn4, const QString &lrn5, const QString &lrn6, const QString &lrn7)
    : prefixe1(prefixe1), prefixe2(prefixe2), prefixe3(prefixe3), prefixe4(prefixe4), prefixe5(prefixe5), prefixe6(prefixe6), prefixe7(prefixe7),
      lrn1(lrn1), lrn2(lrn2), lrn3(lrn3), lrn4(lrn4), lrn5(lrn5), lrn6(lrn6), lrn7(lrn7)
{
    h = new QWidget(this);
    QGridLayout *hl = new QGridLayout;
    resize(500,300);
    h->setLayout(hl);
    setCentralWidget(h);
    setWindowTitle(titre);

    QCheckBox *checkbox_kparam1 = new QCheckBox;    QCheckBox *checkbox_kparam2 = new QCheckBox;
    QCheckBox *checkbox_kparam3 = new QCheckBox;    QCheckBox *checkbox_kparam4 = new QCheckBox;
    QCheckBox *checkbox_kparam5 = new QCheckBox;    QCheckBox *checkbox_kparam6 = new QCheckBox;
    QCheckBox *checkbox_kparam7 = new QCheckBox;

    QList<QCheckBox*> checkBox;
    checkBox <<  checkbox_kparam1 << checkbox_kparam2 << checkbox_kparam3 << checkbox_kparam4 << checkbox_kparam5
             <<  checkbox_kparam6 << checkbox_kparam7;

    int const tailleTableau(7);
    QString checkBoxNames[tailleTableau] = {lrn1 , lrn2 , lrn3 , lrn4 , lrn5 , lrn6 , lrn7};

    Kparam1 = new QDial;    Kparam1->setRange(1,64);    Kparam1->setValue(1);
    Kparam2 = new QDial;    Kparam2->setRange(0,360);   Kparam2->setValue(0);
    Kparam3 = new QDial;    Kparam3->setRange(0,100);   Kparam3->setValue(50);
    Kparam4 = new QDial;    Kparam4->setRange(0,100);   Kparam4->setValue(50);
    Kparam5 = new QDial;    Kparam5->setRange(0,360);   Kparam5->setValue(0);
    Kparam6 = new QDial;    Kparam6->setRange(0,100);   Kparam6->setValue(50);
    Kparam7 = new QDial;    Kparam7->setRange(0,100);   Kparam7->setValue(50);

    QList<QDial*> potard;
    potard << Kparam1 << Kparam2 << Kparam3 << Kparam4 << Kparam5 << Kparam6 << Kparam7;

    QString QDialNames[tailleTableau] = {prefixe1, prefixe2, prefixe3, prefixe4, prefixe5, prefixe6, prefixe7};

    QLabel *KeffectNbreSegments = new QLabel("Number of segments : ");
    QLabel *KeffectRotationInputSegment = new QLabel("Rotation of the input segment : ");
    QLabel *KeffectCenterPositionX = new QLabel("x Position of the segment of the input image: ");
    QLabel *KeffectCenterPositionY = new QLabel("y Position of the segment of the input image : ");
    QLabel *KeffectRotationOutputSegment = new QLabel("Rotation of the output segment : ");
    QLabel *KeffectNormalizedCenterPositionOutputX = new QLabel("x Position of the segment of the output image:  ");
    QLabel *KeffectNormalizedCenterPositionOutputY = new QLabel("y Position of the segment of the output image:  ");

    QList<QLabel*> labels;
    labels << KeffectNbreSegments << KeffectRotationInputSegment << KeffectCenterPositionX << KeffectCenterPositionY
           << KeffectRotationOutputSegment << KeffectNormalizedCenterPositionOutputX << KeffectNormalizedCenterPositionOutputY;

    for (int i=0; i<checkBox.size(); i++)
    {
        checkBox[i]->setObjectName(checkBoxNames[i]);
        potard[i]->setObjectName(QDialNames[i]);
        hl->addWidget(checkBox[i], i , 0);
        hl->addWidget(potard[i], i, 2);
        hl->addWidget(labels[i], i , 1);
        connect(potard[i], SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));
        connect(checkBox[i], SIGNAL(clicked()), this, SLOT(valueChanged()));
    }

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->show();
}

void kaleidoscope::valueChanged(int value)
{
    QObject *A = sender();
    QString _qpd_m;
    if (A)  _qpd_m = A->objectName() + " " + QString::number(value);
    emit kParamChanged(_qpd_m);
    qDebug()<<_qpd_m;
}

void kaleidoscope::valueChanged(void)
{
    QObject *A = sender();
    QString _qpd_m;
    if (A)  _qpd_m = A->objectName();
    emit kParamChanged(_qpd_m);
    qDebug()<<_qpd_m;
}

void kaleidoscope::closeEvent(QCloseEvent *event)
{
  emit close();
  event->accept();
}

/*QSpinBox *kaleidoscope::getKparam1() a garder, c'est pour voir comment on fait un getter
{
    return Kparam1;
}*/




