#include "backlight.h"
#include "qtmini.h"

backlight::backlight(const QString & titre, const QString & prefixe1, const QString & prefixe2, const QString & prefixe3, const QString & lrn1, const QString & lrn2, const QString & lrn3 ):
    prefixe1(prefixe1), prefixe2(prefixe2), prefixe3(prefixe3), lrn1(lrn1), lrn2(lrn2), lrn3(lrn3)
{
    h = new QWidget(this);
    QGridLayout *hl = new QGridLayout;
    resize(300,160);
    h->setLayout(hl);
    setCentralWidget(h);
    setWindowTitle(titre);

    QCheckBox *checkbox_bparam1 = new QCheckBox;
    QCheckBox *checkbox_bparam2 = new QCheckBox;
    QCheckBox *checkbox_bparam3 = new QCheckBox;

    QList<QCheckBox*> checkBox;
    checkBox << checkbox_bparam1 << checkbox_bparam2 << checkbox_bparam3;

    int const tailleTableau(3);
    QString checkBoxNames[tailleTableau] = {lrn1 , lrn2 , lrn3};

    QLabel *Bscale = new QLabel("Scale factor : ");
    QLabel *Bfloor = new QLabel("Floor : ");
    QLabel *Bceiling = new QLabel("Ceiling : ");

    QList <QLabel*> labels;
    labels << Bscale << Bfloor << Bceiling;

    Bparam1 = new QDial;    Bparam1->setRange(0,100);    Bparam1->setValue(0);
    Bparam2 = new QDial;    Bparam2->setRange(0,255);    Bparam2->setValue(0);
    Bparam3 = new QDial;    Bparam3->setRange(0,255);    Bparam3->setValue(0);

    QList<QDial*> potard;
    potard << Bparam1 << Bparam2 << Bparam3;

    QString QDialNames[tailleTableau] = {prefixe1, prefixe2, prefixe3};

    for (int i=0; i<checkBox.size(); i++)
    {
        checkBox[i]->setObjectName(checkBoxNames[i]);
        potard[i]->setObjectName(QDialNames[i]);
        hl->addWidget(checkBox[i], i , 0);
        hl->addWidget(potard[i], i, 2);
        hl->addWidget(labels[i], i , 1);
        connect(potard[i], SIGNAL(valueChanged(int)), this, SLOT(valueChanged(int)));
        connect(checkBox[i], SIGNAL(clicked()), this, SLOT(valueChanged()));
    }

    this->setAttribute(Qt::WA_DeleteOnClose);
    this->show();
}

void backlight::valueChanged(int value)
{
    QObject *A = sender();
    QString _qpd_m;
    if (A)  _qpd_m = A->objectName() + " " + QString::number(value);
    emit kParamChanged(_qpd_m);
    qDebug()<<_qpd_m;
}

void backlight::valueChanged(void)
{
    QObject *A = sender();
    QString _qpd_m;
    if (A)  _qpd_m = A->objectName();
    emit kParamChanged(_qpd_m);
    qDebug()<<_qpd_m;
}

void backlight::closeEvent(QCloseEvent *event)
{
  emit close();
  event->accept();
}
