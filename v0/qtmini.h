#ifndef QTMINI_H
#define QTMINI_H
#include "kaleidoscope.h"
#include "backlight.h"
#include <QtGui>
#include <QtNetwork>

class QtMini : public QMainWindow
{
// La macro Q_OBJECT est indispensable a toute classe heritant de QWidget
    Q_OBJECT

    QTabWidget *m_onglets;
    QWidget *m_page1, *m_page2, *m_page3, *m_page4;
    QWidget *g;
    QSlider *CrossFaderSlider, *R_left, *V_left, *B_left, *A_left, *R_right, *V_right, *B_right, *A_right;
    QPushButton *VideoLeft, *VideoRight, *play1, *play2, *go, *connectButton2, *connectButton, *createButton, /**destroyButton,*/
                *kaleidoscope1, *kaleidoscope2, *backlight1, *backlight2;
    QSpinBox *pd_port_SpinBox2, *pd_port_SpinBox;
    QTextEdit *Console, *viewVideoNameLeft, *viewVideoNameRight;
    QCheckBox *checkbox_R_left, *checkbox_V_left, *checkbox_B_left, *checkbox_A_left, *checkbox_R_right, *checkbox_V_right, *checkbox_B_right, *checkbox_A_right, *CrossFader_checkBox,
    *checkbox_playL, *checkbox_playR, *checkbox_openVR, *checkbox_openVL, *checkbox_create, /**checkbox_destroy,*/ *checkbox_Keffect1, *checkbox_Keffect2, *checkbox_backlight1, *checkbox_backlight2,
    *checkbox_Dol, *checkbox_Dor, *checkbox_Rel, *checkbox_Rer, *checkbox_Mil, *checkbox_Mir, *checkbox_Fal, *checkbox_Far, *checkbox_Soll,
    *checkbox_Solr, *checkbox_Lal, *checkbox_Lar, *checkbox_Sil, *checkbox_Sir;
    kaleidoscope *k1, *k2;
    backlight *b1, *b2;
    QPushButton *chooseLeftVid, *chooseRightVid, *resetRightVid, *resetLeftVid;
    QList<QSlider*> Sliders;

public :
    QtMini() ;
    QTcpServer *qpd_Serv2;
    QTcpSocket *socket2;
    QTcpSocket *qpd_Sock;

public slots:
    void connection2();
    void read2();
    void qpd_tcp_sock();
    void qpd_tcp_sock(int value);
    void qpd_tcp_w(QString _qpd_m);
    void connectToHost();
    void openWindow();
    void activateButton();
    void openInstructionWindow();

signals :
    void _qpd_new(QString _M);
    void _qpd_new8(QString);
    void _qpd_new2(QString);
    void clicked(QString);
    void emitFichierName(const QString &);

};

#endif // QTMINI_H
