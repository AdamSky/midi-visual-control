QT += network


SOURCES += \
    main.cpp \
    qtmini.cpp \
    backlight.cpp \
    kaleidoscope.cpp

HEADERS += \
    qtmini.h \
    backlight.h \
    kaleidoscope.h

RESOURCES +=
