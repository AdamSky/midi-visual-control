#include <QApplication>

#include "qtmini.h"
int main(int argc, char **argv)
{
    //Tout programme Qt necessite de creer un objet Qapplication
    QApplication app(argc, argv, "application Qt minimale");
    QtMini *wid = new QtMini;
    wid->setWindowTitle("MIDI Visual Control");
   /*QFile File("qmc2-black-0.10/qmc2-black-0.10.qss");
     File.open(QFile::ReadOnly);
     QString StyleSheet = QLatin1String(File.readAll());
     qApp->setStyleSheet(StyleSheet);*/
    wid->show();

    //la totalite de l'execution de l'application se deroule dans la methode exec() de Qapplication
    return app.exec();
}
