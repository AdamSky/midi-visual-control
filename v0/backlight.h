#ifndef BACKLIGHT_H
#define BACKLIGHT_H

#include <QtGui>

class backlight : public QMainWindow
{
    Q_OBJECT
    QWidget *h;
    QDial *Bparam1, *Bparam2, *Bparam3;

private:
    QString prefixe1, prefixe2, prefixe3, lrn1, lrn2, lrn3;
public:
    backlight(const QString &titre, const QString &prefixe1, const QString &prefixe2, const QString &prefixe3 , const QString &lrn1, const QString &lrn2, const QString &lrn3);
    void closeEvent(QCloseEvent *event);
signals:
    void kParamChanged(const QString &);
    void close();

private slots:
    void valueChanged(int);
    void valueChanged(void);
};

#endif // BACKLIGHT_H
